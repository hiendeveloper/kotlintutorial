package com.example.ben.kotlintutorial

import android.text.TextUtils

class LoginPresenter(private val view: LoginView) {

    fun onLoginClicked() {
        val email:String = view?.getEmailUser ?: ""
        val pass:String = view?.getPasswordUser ?: ""
        if (TextUtils.isEmpty(email)) {
            view?.onLoginError(R.string.error_email_empty)
            return
        }
        if (TextUtils.isEmpty(pass)) {
            view?.onLoginError(R.string.error_password_empty)
            return
        }
        if (email.contentEquals("abc") && pass.contentEquals("123"))
            view?.onLoginSuccess()
        else
            view?.onLoginError(R.string.error_email_or_password_valid)

    }

}