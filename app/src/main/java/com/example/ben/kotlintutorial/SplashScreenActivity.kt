package com.example.ben.kotlintutorial

import android.app.Activity
import android.os.Bundle

class SplashScreenActivity: Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LoginActivity.startActivity(this)
    }

}