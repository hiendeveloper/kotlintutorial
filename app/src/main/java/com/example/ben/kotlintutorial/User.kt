package com.example.ben.kotlintutorial

data class User(val name: String, val title: String)