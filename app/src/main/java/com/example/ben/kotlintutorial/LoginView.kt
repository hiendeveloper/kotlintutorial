package com.example.ben.kotlintutorial

interface LoginView {

    val getEmailUser: String
    val getPasswordUser: String

    fun onLoginError(error:Int)
    fun onLoginSuccess()

}