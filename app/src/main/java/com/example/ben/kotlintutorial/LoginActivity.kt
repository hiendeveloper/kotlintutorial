package com.example.ben.kotlintutorial

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
class LoginActivity() : BaseActivity(), LoginView {

    override val getEmailUser: String
        get() = edEmail.text.toString() //To change initializer of created properties use File | Settings | File Templates.

    override val getPasswordUser: String
        get() = edPass.text.toString() //To change initializer of created properties use File | Settings | File Templates.

    override fun onLoginError(error: Int) {
        applicationContext.toast(getString(error))
    }

    override fun onLoginSuccess() {
        applicationContext.toast("Success")
    }

    companion object {
        fun startActivity(activity: Activity) {
            val intent = Intent(activity, LoginActivity::class.java)
            activity.startActivity(intent)
        }
    }

    var loginPresenter: LoginPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginPresenter = LoginPresenter(this)
        setContentView(R.layout.activity_login)
        btnLoginSubmit.setOnClickListener{ loginPresenter?.onLoginClicked() }
    }

}